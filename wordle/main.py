import random
from colorama import init, Fore, Back, Style
init(autoreset=True)

size = 5

# wordList = ["hello", "paint", "stand", "proxy", "mince", "house",
#             "mouse", "prank", "stern", "whack", "begin", "catch", "drown", "trees"]
words = open("wordlist.txt", "r")
content = words.read()
wordList = content.split("\n")
words.close()
GREY = "\x1b[1;30;40m"
RED = "\x1b[1;31;40m"
GREEN = "\x1b[1;32;40m"
YELLOW = "\x1b[1;33;40m"
WHITE = "\x1b[1;37;40m"

alpha = 'abcdefghijklmnopqrstuvwxyz'
r = random.randrange(1, len(wordList), 1)
# ans = wordList[r]
ans = "bless"
alphaColorArr = []
guessedLetters = []
wordColorArr = []
outArr = []
for i in range(size):
    wordColorArr.append(WHITE)
for i in range(26):
    alphaColorArr.append(WHITE)


def getGuessedLetters(word, guessedLetters):
    for letter in word:
        if not (letter in guessedLetters):
            guessedLetters += letter


# def changeColors(guessedLetters):
#     for el in guessedLetters:
#         if el in ans:
#             if word.index(el) == ans.index(el):
#                 alphaColorArr[alpha.index(el)] = GREEN
#             else:
#                 alphaColorArr[alpha.index(el)] = YELLOW
#         else:
#             alphaColorArr[alpha.index(el)] = GREY


def changeColors(guessedLetters):
    for el in guessedLetters:
        if el in ans:
            alphaColorArr[alpha.index(el)] = GREEN
        else:
            alphaColorArr[alpha.index(el)] = GREY


def colorWord(word):
    out = ''
    ansArr = []
    for i in range(size):
        ansArr.append(0)
    for i, letter in enumerate(word):
        if letter in ans:
            if ans.count(letter) > 1:
                posAnsArr = []
                posWordArr = []
                for i in range(len(ans)):
                    if letter == ans[i]:
                        posAnsArr.append(i)
                    if letter == word[i]:
                        posWordArr.append(i)
                for i, letter in enumerate(posWordArr):
                    if i+1 > len(posAnsArr):
                        ansArr[letter] = 0
                    elif letter in posAnsArr:
                        ansArr[letter] = 2
                    else:
                        ansArr[letter] = 1

            else:
                if word.index(letter) == ans.index(letter):
                    # wordColorArr[i] = GREEN
                    # out += GREEN + letter + "\x1b[0m"
                    ansArr[i] = 2
                else:
                    # wordColorArr[i] = YELLOW
                    # out += YELLOW + letter + "\x1b[0m"
                    ansArr[i] = 1
        else:
            # wordColorArr[i] = RED
            # out += RED + letter + "\x1b[0m"
            ansArr[i] = 0
    print("Word array = ", posWordArr)
    print("Ans array = ", posAnsArr)
    print("Output array = ", ansArr)
    for i, pos in enumerate(ansArr):
        if pos == 0:
            out += RED + word[i] + "\x1b[0m"
        elif pos == 1:
            out += YELLOW + word[i] + "\x1b[0m"
        else:
            out += GREEN + word[i] + "\x1b[0m"
    outArr.append(out)
    for i in outArr:
        print(i)


# print("The word is " + Fore.CYAN + ans)
# print(Fore.RED + "Red text " + Fore.GREEN + "Green text")
# print(Fore.WHITE + "White text")
win = False
while not win:
    valid = False
    word = input("Enter a word: ")

    while len(word) != size:
        word = input("Enter a 5 letter word: ")
    if word == ans:
        win = True
        colorWord(word)
        print("You won in " + str(len(outArr)) + " guesses!")
        break
    colorWord(word)
    getGuessedLetters(word, guessedLetters)
    changeColors(guessedLetters)
    final = ""
    for i, el in enumerate([alphaColorArr[i] + alpha[i] + "\x1b[0m" for i, el in enumerate(alpha)]):
        final += el
    print(final)

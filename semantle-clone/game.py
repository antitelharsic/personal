import os
import gensim.downloader as api
from rich.console import Console
from rich.table import Column, Table
from rich.text import Text
import colorsys
import random
from numpy import clip
import string

# Dependancies
#
# pip install rich
# https://rich.readthedocs.io/
#
# pip install gensim
# https://pypi.org/project/gensim/

console = Console()

# Define variables

# How big do you want the word space to be?
# (This only refers to the answers, not guesses)
# 10000 = somewhat uncommon
# 5000 = fairly common
# 2500 = common
# 1000 = well known
# 500 = very well known
# 250 = simple
vocabSize = 2500

# Which word database and model do you want to use?
# (Some have more words and more dimenions)
# Larger numbers will take up more space
# glove-wiki-gigaword-50 (65 MB)
# glove-wiki-gigaword-100 (128 MB)
# glove-wiki-gigaword-200 (252 MB)
# glove-wiki-gigaword-300 (376 MB)
# glove-twitter-25 (104 MB)
# glove-twitter-50 (199 MB)
# glove-twitter-100 (387 MB)
# glove-twitter-200 (758 MB)

glove = "glove-wiki-gigaword-50"

# Cheat mode: Hide or show answer
cheat = True

# Source folder
src = "src/"

# All answers will be stored here
answersfile = src+"allanswers.txt"

# Dictionary and helper files
dictionaryfile = src+"dict.txt"
prepfile = src+"prepositions.txt"
pronounfile = src+"pronouns.txt"
miscfile = src+"misc.txt"
firstnamesfile = src+"first-names.txt"
lastnamesfile = src+"last-names.txt"
dictSize = sum(1 for line in open(dictionaryfile))
prons = []
vocab = []
preps = []
names = []
misc = []

# Cap the vocabulary size at the size of the dictionary
vocabSize = clip(vocabSize, 10, dictSize)

# Read the pronouns, prepositions, names, and misc words
# to eliminate them from the vocab list
with open(miscfile, encoding="utf8") as file:
    for line in file:
        misc.append(line.split()[0].lower())
with open(pronounfile, encoding="utf8") as file:
    for line in file:
        prons.append(line.split()[0].lower())
with open(prepfile, encoding="utf8") as file:
    for line in file:
        preps.append(line.split()[0].lower())
with open(firstnamesfile, encoding="utf8") as file:
    for line in file:
        names.append(line.split()[0].lower())
with open(lastnamesfile, encoding="utf8") as file:
    for line in file:
        names.append(line.split()[0].lower())

# Filter out the useless words from the dictionary
with open(dictionaryfile, 'r', encoding="ISO-8859-1") as file:
    for i in range(vocabSize):
        w = next(file).strip()
        flag = 1
        if len(w) > 2 and (not w in (preps + prons + misc + names)):
            for el in w:
                if el in string.punctuation + string.digits:
                    flag = 0
            if flag == 1:
                vocab.append(w)

console.print("Size of vocab = " + str(len(vocab)))
# console.print(vocab[:100])


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def pickWord():
    c = ""
    while len(c) <= 1:
        # r = random.randrange(len(vocabShort)-1000, len(vocabShort), 1)
        # r = random.randrange(0, 500, 1)
        c = random.choice(vocab)
        # c = vocabShort[r]
        # print(c)

    # Store chosen word in text file
    f = open(answersfile, "a")
    f.write("\n" + c)
    f.close()
    return c


def getColor(simil):
    color = tuple(round(i * 255)
                  for i in colorsys.hls_to_rgb(simil/100*120, 0.66, 0.50))
    colorString = "rgb(" + str(color[0]) + "," + \
        str(color[1]) + "," + str(color[2]) + ")"
    return colorString


def createTable(currentGuess):
    # cls()
    console.print("Guesses so far: " + str(len(output)))
    table = Table(show_header=True, header_style="bold magenta")
    table.add_column("Guess", style="dim", width=12)
    table.add_column("Similarity")
    table.add_column("Getting Close?", justify="left")
    for el in output:
        pos = el[2]
        g = el[0]
        s = el[1]
        if pos == -1:
            colorNum = clip(round(s/5)-1, 0, 9)
            guessText = Text(g, style="bright_red" if g ==
                             currentGuess else colors[colorNum])
            table.add_row(guessText, str(s))
        elif pos == -2:
            guessText = Text(g, style="bright_red" if g ==
                             currentGuess else "bright_black")
            closeText = Text("Not in List", style="red")
            table.add_row(guessText, str(s), closeText)
        else:
            green = round((1000-pos)/100)
            colorNum = clip(round(s/5)-1, 0, 9)
            guessText = Text(g, style="bright_red" if g ==
                             currentGuess else colors[colorNum])
            closeText = Text("           " + str(1000-pos)+"/1000")
            closeText.stylize("black on green", 0, green)
            closeText.stylize("white on gray", green+1, 10)
            table.add_row(guessText, str(s), closeText)
    console.print(table)


# Shows top X close words to answer
# Change X to get more or less words
closeWordCount = 10


def showCloseWords():
    for i in range(closeWordCount):
        console.print(str(i+1) + ": " + str(close[i][0]))


def endScene():
    f = open(answersfile, "a")
    f.write(" " + tries)
    f.close()
    seeCloseWords = console.input(
        "Do you want to see the [cyan]" + str(closeWordCount) + "[/cyan] closest words? ").lower()
    if seeCloseWords in ["y", "yes", ""]:
        showCloseWords()


colors = ["bright_black",
          "white",
          "bright_white",
          "yellow",
          "bright_yellow",
          "magenta",
          "bright_magenta",
          "cyan",
          "bright_cyan",
          "bright_green"]

output = []
won = False
wv = api.load(glove)
word = pickWord()
# word = "vital"
if cheat:
    console.print("Word: [green]" + word + "[green]")
close = wv.similar_by_word(word, 400000)
second = round(close[0][1]*100, 2)
console.print(
    "Second closest word has a score of: [green]" + str(second) + "[/green]")
console.print("Type [red]123[/red] at any time to give up!")
close1000 = close[:1000]
giveUp = False

while not won:
    goodInput = False
    while not goodInput:
        guess = console.input("What's your guess? ").lower()
        if guess == "123":
            goodInput = True
            giveUp = True
        elif len(guess.split(" ")) > 1:
            console.print(
                "You can only pick [cyan]one[/cyan] word guesses. [red]Try again[/red]")
        elif any(guess in el for el in output):
            console.print(
                "You already guessed that word! [red]Try again[/red]")
        else:
            goodInput = True
    tries = str(len(output)+1)
    if giveUp:
        console.print("The word was: [green]" + word + "[/green]")
        endScene()
        break
    elif guess == word:
        console.print("[green]Congrats![/green] You got it in [cyan]" +
                      tries + "[/cyan] tries")
        endScene()
        break
    simil = -50
    pos = -2
    for el in close:
        if guess == el[0]:
            simil = round(el[1]*100, 2)
            # console.print("Similarity: " + str(simil))
            pos = -1
    if any(guess in el for el in close1000):
        console.print("Found it!")
        for i, el in enumerate(close1000):
            if guess == el[0]:
                console.print("The position is: " + str(i+1))
                pos = i+1
    output += [[guess, simil, pos]]
    output = sorted(output, key=lambda x: x[1], reverse=True)
    createTable(guess)
